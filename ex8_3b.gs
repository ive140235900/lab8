function changeColor() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sh = ss.getSheetByName('eng_league');
  var dataRange = sh.getDataRange();
  var color = 'yellow';
  setAlternateRowsColor(dataRange, color);
}

function setAlternateRowsColor(range, color) {
  if (range.toString() !== 'Range') {
    throw {
      'name': 'TypeError',
      'message': '1st argument must be type Range'
    }
  }
  
  if (typeof color !== 'string') {
    throw {
      'name': 'TypeError',
      'message': '2nd argument must be a string for color'
    }
  }
  
  var startCell = range.getCell(1,1);
  var columnCount = range.getLastColumn();
  var lastRow = range.getLastRow();
  var selectedRange;
  
  for (var i=0; i < lastRow; i++ ) {
    if ( i % 2 ) {
      selectedRange = startCell.offset(i, 0, 1, columnCount);
      selectedRange.setBackgroundColor(color);
    }
  }
}