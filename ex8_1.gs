// --------------------------------
function sayHelloBrowser() {
    var greeting = 'Hello world!';
    // Display a message dialog with the greeting
    // (visible from the containing spreadsheet
    Browser.msgBox(greeting);
}
// --------------------------------
function helloDocument() {
    var greeting = 'Hello world!';
    // Create DocumentApp instance.
    var doc = DocumentApp.create('test_DocumentApp');
    // Write the greeting to a Google document.
    doc.setText(greeting);
    // Close the newly created document
    doc.saveAndClose();
}
// --------------------------------
function helloLogger() {
    var greeting = 'Hello world!';
    // Write the greeting to a logging window.
    // This is visible from the script editor
    // window menu "View->Logs...".
    Logger.log(greeting);
}
// --------------------------------
function helloSpreadsheet() {
    var greeting = 'Hello world!';
    var sheet = SpreadsheetApp.getActiveSheet();
    // Post the greeting variable value to cell A1
    // of the active sheet in the containing spreadsheet.
    sheet.getRange('A1').setValue(greeting);
}