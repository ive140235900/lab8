// Function to demonstrate the Spreadsheet
// object hierarchy.
function showGoogleSpreadsheetHierarchy() {
    // return a Spreadsheet object
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    // return the Spreadsheet Id
    var id = ss.getId();
    // return a Sheet object
    var sh = ss.getActiveSheet();
    // return an active cell selected in the spreadsheet
    var acell = sh.getActiveCell();
    // return the active cell address
    var acellAddress = acell.getA1Notation();
    // returns a Range object for the given address A1:C10
    var rng = sh.getRange('A1:C10');
    // returns the cell object of row-8 and col-3 ("C8")
    var innerRng = rng.getCell(8, 3);
    // returns the range address as a string, i.e. "C8"
    var innerRngAddress = innerRng.getA1Notation();
    var googleObjects = [ss, id, sh, acell, acellAddress, rng, innerRng, innerRngAddress];
    for (var i=0; i<googleObjects.length; i+=1) {
        Logger.log(googleObjects[i].toString());
    }
}