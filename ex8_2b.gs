function rangeOffsetDemo() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sh = ss.getActiveSheet();
  var rng = sh.getRange('C10');
  
  rng.setBackground('red');
  rng.setValue('Method offset()');
  
  rng.offset(-1, -1).setValue('(-1, -1)');
  rng.offset(-1, -1).setComment('top-left corner');
  
  rng.offset(-1, 1).setValue('(-1, 1)');
  rng.offset(-1, -1).setComment('top-right corner');
  
  rng.offset(1, -1).setValue('(1, -1)');
  rng.offset(1, -1).setComment('bottom-left corner');
  
  rng.offset(1, 1).setValue('(1, 1)');
  rng.offset(1, 1).setComment('bottom-right corner');
  
  dataRange = sh.getDataRange();
  Logger.log(dataRange.getA1Notation());
}
