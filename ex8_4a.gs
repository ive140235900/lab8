function sendmail() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sh = ss.getActiveSheet();
  var rng = sh.getRange('A2:B3');
  
  var data = rng.getValues();
  
  for ( i in data ) {
    var row = data[i];
    var emailAddress = row[0];
    var message = row[1];
    var subject = "Sending emails from Google App Script";
    
    MailApp.sendEmail(emailAddress, subject, message);
  }
}